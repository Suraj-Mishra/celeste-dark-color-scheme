# Celeste Dark Color Scheme

Celeste color scheme with dark variables. The aim of this project is to create and maintain a dark variant for celeste theme with hashed syntax applied.

```
{
	"variables":
	{
        "folly": "#ff004f",
        "cyan": "#00cfff"
	},``
	"globals":
	{
		"background": "black",
		"line_highlight": "rgba(255, 255, 255, .15)",
		"gutter": "black",
		"gutter_foreground": "white",
		"gutter_foreground_highlight": "var(cyan)"
	},
	"rules":
	[
        {
            "scope": "source - comment - string - keyword - punctuation - storage - entity - source.css",
            "foreground": ["var(green)", "var(cyan)"]
        },
		{
            "scope": "entity.name - entity.name.tag - entity.name.section",
            "foreground": "var(orange)",
            "background": "black",
        },
        {
            "scope": "entity.other.inherited-class",
            "foreground": "var(dark_orange)",
            "background": "black",
        },
        {
            "scope": "keyword.operator",
            "foreground": "var(magenta)",
            "background": "black"
        },
        {
            "scope": "storage.type.numeric",
            "foreground": "var(blue)",
            "background": "black",
            "font_style": "italic"
        },
        {
            "scope": "storage, support.type.sys-types",
            "foreground": "var(folly)",
            "background": "black",
            "font_style": "italic"
        },
        {
            "scope": "keyword, constant.language",
            "foreground": "var(red)",
            "font_style": "italic"
        },
	]
}
```

